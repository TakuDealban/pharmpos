<?php
include_once("init.php");
require 'db/dbapi.php';

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>WEBPOS - Sales</title>

    <!-- Stylesheets -->
    <!---->
  
       <link rel="stylesheet" href="css/style.css">
   
 <link rel="stylesheet" href="js/datatables/jquery.dataTables.min.css">
<link rel="stylesheet" href="js/datatables/jquery.dataTables.min.css">
<link rel="stylesheet" href="js/datatables/buttons.dataTables.min.css">

    <!-- Optimize for mobile devices -->
         <?php include_once("tpl/common_js.php"); ?>
    <script src="js/datatables/jquery.dataTables.min.js"></script>
<script src="js/datatables/dataTables.buttons.min.js"></script>
<script src="js/datatables/buttons.flash.min.js"></script>
<script src="js/datatables/jszip.min.js"></script>
<script src="js/datatables/pdfmake.min.js"></script>
<script src="js/datatables/vfs_fonts.js"></script>
<script src="js/datatables/buttons.html5.min.js"></script>
<script src="js/datatables/buttons.print.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    
</head>
<body>

<!-- TOP BAR -->
<?php //include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->





<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

    <div class="side-menu fl">

<h3>SKU Report</h3>
<ul>
    <li><a href="view_report.php">Back</a></li>
    
</ul>

</div>

        <div class="side-content  fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    
                    <h3 class="fl">Sales</h3>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">
            <table class="table table-borderless table-center js-dataTable-full ">
                                <thead>
                                     <tr>
                                        <th>Date</th>
                                    <th>Stock Item</th>
                                    <th>Quantity Sold</th>
                                    <th>Selling Price</th>
                                    <th>Amount Paid</th>
                                    

                                </tr>
                                </thead>
                                <tbody>
                                    
                               
                               

                                <?php 
                            $DateFrom = $_GET["from"];
                            $DateTo = $_GET["to"];
                             $sales =   getSkuSales($DateFrom,$DateTo);
                             $TotQty = 0;
                             $TotAmtPaid = 0;

                             foreach($sales as $row){
                                 
                                  $TransactionDate = date("d M y",strtotime($row["CreatedDate"]));
                                  $Prod = $row["stock_id"]." - ".$row["stock_name"]; 
                                  $Qty = $row["quantity"]; 
                                  $Price = $row["selling_price"];
                                  $AmountPaid = $row["subtotal"]; 

                                $TotQty += $Qty;
                                $TotAmtPaid  += $AmountPaid;

                                  
                                    ?>
                                    <tr>
                                        <td> <?php echo $TransactionDate; ?></td>
                                        <td> <?php echo $Prod; ?></td>
                                        <td> <?php echo  $Qty; ?></td>
                                         <td> <?php echo $Price ; ?></td>
                                         <td> <?php echo $AmountPaid ; ?></td>
                                         
                                      

                                    </tr>

                                    
                                  
                               <?php } 
                               
                               
                               ?>
                               
                                     </tbody>
<tfoot>
                                     <tr>
                                        <td> </td>
                                        <td> <strong> SUMMARY : TOTALS </strong></td>
                                        <td> <strong><?php echo  $TotQty; ?></strong></td>
                                         <td> <strong><?php echo "" ; ?></strong></td>
                                         <td> <strong><?php echo $TotAmtPaid ; ?></strong></td>
                                         
                                      

                                    </tr>
                                    </tfoot>
                              
                            </table>


                </div>
            </div>
            <div id="footer">
                <p>Any Queries send to <a href="mailto:takundamadzamba@gmail.com">+263773629282</a>.
                </p>

            </div>
            
 <script>
                 $(document).ready(function () {
  jQuery('.js-dataTable-full').dataTable({
            columnDefs: [ { orderable: false, targets: [ 2 ] } ],
            pageLength: 20,
             "order": [[ 2, "asc" ]],
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
             dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        });
     
    });
                </script>
            <!-- end footer -->

</body>
</html>