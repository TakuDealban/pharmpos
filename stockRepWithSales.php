<?php
include_once("init.php");
require 'db/dbapi.php';

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>WEBPOS - Sales</title>

    <!-- Stylesheets -->
    <!---->
  
       <link rel="stylesheet" href="css/style.css">
   
 <link rel="stylesheet" href="js/datatables/jquery.dataTables.min.css">
<link rel="stylesheet" href="js/datatables/jquery.dataTables.min.css">
<link rel="stylesheet" href="js/datatables/buttons.dataTables.min.css">

    <!-- Optimize for mobile devices -->
         <?php include_once("tpl/common_js.php"); ?>
    <script src="js/datatables/jquery.dataTables.min.js"></script>
<script src="js/datatables/dataTables.buttons.min.js"></script>
<script src="js/datatables/buttons.flash.min.js"></script>
<script src="js/datatables/jszip.min.js"></script>
<script src="js/datatables/pdfmake.min.js"></script>
<script src="js/datatables/vfs_fonts.js"></script>
<script src="js/datatables/buttons.html5.min.js"></script>
<script src="js/datatables/buttons.print.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    
</head>
<body>

<!-- TOP BAR -->
<?php //include_once("tpl/top_bar.php");

                            $DateFrom = $_GET["fromStkRepDate"];
                            $DateTo = $_GET["to_stock_date"];
                          
                           
?>
<!-- end top-bar -->





<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

    <div class="side-menu fl">

<h3>SKU Report</h3>
<ul>
    <li><a href="view_report.php">Back</a></li>
    
</ul>

</div>

        <div class="side-content  fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    
                    <h3 class="fl">Stock Report With SAles - <?php echo $DateFrom. " - ".$DateTo; ?></h3>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">
            <table class="table table-borderless table-center js-dataTable-full ">
                                <thead>
                                     <tr>
                                    <th>Product Code</th>
                                    <th>Product Description</th>
                                    <th>Cost Of Purchase</th>
                                    <th>Stock On Hand</th>
                                    <th>Selling Price</th>
                                    <th>Period Sales</th>
                                    <th>Total Sales $</th>
                                    

                                </tr>
                                </thead>
                                <tbody>
                                    
                               
                               

                                <?php 
                     
                             $stocks =   stockReport();
                             $TotStockOnHand = 0;
                             $TotSalesQty = 0;
                             $TotSalesValue = 0;
                             foreach($stocks as $row){
                                 

                                $ProdCode = $row["stock_id"];
                                $ProdDesc = $row["stock_name"];
                                $ProdQty = $row["stock_quatity"];
                                $ProdPrice = $row["company_price"];
                                $ProdQty = $row["stock_quatity"];
                                $SellingPrice = $row["selling_price"];
                                $PeriodSales = getSkuSalePerPrd($DateFrom,$DateTo,$ProdCode);

                                $TotQtySold = $PeriodSales[0]["totQty"];
                                $TotRevSold = $PeriodSales[0]["TotRev"];

                                $TotStockOnHand += $ProdQty;
                                $TotSalesQty += $TotQtySold;
                                $TotSalesValue += $TotRevSold;
                                  
                                    ?>
                                    <tr>
                                        <td> <?php echo $ProdCode; ?></td>
                                        <td> <?php echo $ProdDesc; ?></td>
                                        <td> <?php echo $ProdPrice ; ?></td>
                                        <td> <?php echo  $ProdQty; ?></td>
                                        <td> <?php echo $SellingPrice ; ?></td>
                                        <td> <?php echo $TotQtySold ; ?></td>
                                        <td> <?php echo $TotRevSold ; ?></td>
                                      

                                    </tr>

                                    
                                  
                               <?php } 
                               
                               
                               ?>
                               
                                     </tbody>
<tfoot>
                                     <tr>
                                        <td> </td>
                                        <td> <strong> SUMMARY : TOTALS </strong></td>
                                        <td> <strong><?php echo  ""; ?></strong></td>
                                         <td> <strong><?php echo $TotStockOnHand ; ?></strong></td>
                                         <td> <strong><?php echo "" ; ?></strong></td>
                                         <td> <strong><?php echo $TotSalesQty ; ?></strong></td>
                                         <td> <strong><?php echo $TotSalesValue ; ?></strong></td>

                                         
                                      

                                    </tr>
                                    </tfoot>
                              
                            </table>


                </div>
            </div>
            <div id="footer">
                <p>Any Queries send to <a href="mailto:takundamadzamba@gmail.com">+263773629282</a>.
                </p>

            </div>
            
 <script>
                 $(document).ready(function () {
  jQuery('.js-dataTable-full').dataTable({
            columnDefs: [ { orderable: false, targets: [ 2 ] } ],
            pageLength: 20,
             "order": [[ 2, "asc" ]],
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
             dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        });
     
    });
                </script>
            <!-- end footer -->

</body>
</html>