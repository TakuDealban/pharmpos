

 <ul id="tabs" class="fl">
            <li><a href="dashboard" class="active-tab dashboard-tab">Dashboard</a></li>
            <li><a href="view_sales" class="sales-tab">Sales</a></li>
            <li><a href="view_customers" class=" customers-tab">Customers</a></li>
            <li><a href="view_purchase" class="purchase-tab">Purchase</a></li>
            <li><a href="view_supplier" class=" supplier-tab">Supplier</a></li>
            <li><a href="view_product" class=" stock-tab">Stocks / Products</a></li>
            <li><a href="view_payments" class="payment-tab">Payments</a></li>
            <li><a href="view_report" class="report-tab">Reports</a></li>
            <li><a href="vwBelowMOQ" class="report-tab">MOQ Exceptions</a></li>
        </ul>