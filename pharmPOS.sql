-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 05, 2019 at 01:26 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmPOS`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_details`
--

CREATE TABLE `category_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(120) NOT NULL,
  `category_description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_details`
--

INSERT INTO `category_details` (`id`, `category_name`, `category_description`) VALUES
(1, 'Foods', 'Foods');

-- --------------------------------------------------------

--
-- Table structure for table `customer_details`
--

CREATE TABLE `customer_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_name` varchar(200) NOT NULL,
  `customer_address` varchar(500) NOT NULL,
  `customer_contact1` varchar(100) NOT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `balance` int(11) NOT NULL,
  `status` varchar(20) DEFAULT 'active',
  `Date_Created` datetime DEFAULT CURRENT_TIMESTAMP,
  `Created_BY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_details`
--

INSERT INTO `customer_details` (`id`, `customer_name`, `customer_address`, `customer_contact1`, `Email`, `balance`, `status`, `Date_Created`, `Created_BY`) VALUES
(1, 'Takunda', '11 Watermeyer Drive Belvedere', '0773629282', 'tmadzamba@axissol.com', 0, 'active', '2017-03-15 12:17:58', 1),
(2, 'Masendeke', 'hefeiles', '09002', NULL, 0, 'active', '2018-01-02 23:09:45', 1),
(3, 'Taku Madzamba', '11 Watermeyer drive belvedere Zimbabwe', '', NULL, 0, 'active', '2018-01-05 13:00:21', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pos_response`
--

CREATE TABLE `pos_response` (
  `id` int(6) NOT NULL,
  `response_code` varchar(2) DEFAULT NULL,
  `response` varchar(52) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pos_response`
--

INSERT INTO `pos_response` (`id`, `response_code`, `response`) VALUES
(1, '00', 'Approved or completed successfully'),
(2, '01', 'Refer to card issuer'),
(3, '02', 'Refer to card issuer, special condition'),
(4, '03', 'Invalid merchant'),
(5, '04', 'Pick-up card'),
(6, '05', 'Do not honor'),
(7, '06', 'Error'),
(8, '07', 'Pick-up card, special condition'),
(9, '08', 'Honor with identification'),
(10, '09', 'Request in progress'),
(11, '10', 'Approved, partial'),
(12, '11', 'Approved, VIP'),
(13, '12', 'Invalid transaction'),
(14, '13', 'Invalid amount'),
(15, '14', 'Invalid card number'),
(16, '15', 'No such issuer'),
(17, '16', 'Approved, update track 3'),
(18, '17', 'Customer cancellation'),
(19, '18', 'Customer dispute'),
(20, '19', 'Re-enter transaction'),
(21, '20', 'Invalid response'),
(22, '21', 'No action taken'),
(23, '22', 'Suspected malfunction'),
(24, '23', 'Unacceptable transaction fee'),
(25, '24', 'File update not supported'),
(26, '25', 'Unable to locate record'),
(27, '26', 'Duplicate record'),
(28, '27', 'File update edit error'),
(29, '28', 'File update file locked'),
(30, '29', 'File update failed'),
(31, '30', 'Format error'),
(32, '31', 'Bank not supported'),
(33, '32', 'Completed partially'),
(34, '33', 'Expired card, pick-up'),
(35, '34', 'Suspected fraud, pick-up'),
(36, '35', 'Contact acquirer, pick-up'),
(37, '36', 'Restricted card, pick-up'),
(38, '37', 'Call acquirer security, pick-up'),
(39, '38', 'PIN tries exceeded, pick-up'),
(40, '39', 'No credit account'),
(41, '40', 'Function not supported'),
(42, '41', 'Lost card'),
(43, '42', 'No universal account'),
(44, '43', 'Stolen card'),
(45, '44', 'No investment account'),
(46, '51', 'Not sufficient funds'),
(47, '52', 'No check account'),
(48, '53', 'No savings account'),
(49, '54', 'Card expired or not yet effective'),
(50, '55', 'Incorrect PIN'),
(51, '56', 'No card record'),
(52, '57', 'Transaction not permitted to cardholder'),
(53, '58', 'Transaction not permitted on terminal'),
(54, '59', 'Suspected fraud'),
(55, '60', 'Contact acquirer'),
(56, '61', 'Exceeds withdrawal limit'),
(57, '62', 'Restricted card'),
(58, '63', 'Security violation'),
(59, '64', 'Original amount incorrect'),
(60, '65', 'Exceeds withdrawal frequency'),
(61, '66', 'Call acquirer security'),
(62, '67', 'Hard capture'),
(63, '68', 'Response received too late'),
(64, '75', 'PIN tries exceeded'),
(65, '77', 'Intervene, bank approval required'),
(66, '78', 'Intervene, bank approval required for partial amount'),
(67, '90', 'Cut-off in progress'),
(68, '91', 'Issuer or switch inoperative'),
(69, '92', 'Routing error'),
(70, '93', 'Violation of law'),
(71, '94', 'Duplicate transaction'),
(72, '95', 'Reconcile error'),
(73, '96', 'System malfunction'),
(74, '98', 'Exceeds cash limit');

-- --------------------------------------------------------

--
-- Table structure for table `prod_audit`
--

CREATE TABLE `prod_audit` (
  `audit_id` int(11) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `prod_id` int(11) DEFAULT NULL,
  `DoneDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `DoneBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prod_audit`
--

INSERT INTO `prod_audit` (`audit_id`, `Description`, `prod_id`, `DoneDate`, `DoneBy`) VALUES
(1, 'Sale of 1 units for Fish Capenta', 1, '2017-03-15 12:17:58', 1),
(2, 'Sale of 10 units for Fish Capenta', 1, '2017-03-21 09:54:01', 1),
(3, 'Sale of 10 units for Fish Capenta', 1, '2017-03-21 09:57:54', 1),
(4, 'Sale of 1 units for Fish Capenta', 1, '2017-03-21 09:59:44', 1),
(5, 'Sale of 2 units for Fish Capenta', 1, '2017-03-21 10:00:26', 1),
(6, 'Sale of 1 units for Fish Capenta', 1, '2017-03-21 10:03:38', 1),
(7, 'Sale of 2 units for Fish Capenta', 1, '2017-03-21 10:11:14', 1),
(8, 'Sale of 2 units for Fish Capenta', 1, '2017-03-21 10:11:37', 1),
(9, 'Sale of 2 units for Fish Capenta', 1, '2017-03-21 10:12:08', 1),
(10, 'Sale of 1 units for Fish Capenta', 1, '2017-03-21 11:16:06', 1),
(11, 'Sale of 1 units for Fish Capenta', 1, '2017-03-21 11:16:37', 1),
(12, 'Sale of 1 units for Fish Capenta', 1, '2017-03-21 11:17:59', 1),
(13, 'Sale of 2 units for Fish Capenta', 1, '2017-03-21 11:20:51', 1),
(14, 'Sale of 1 units for Fish Capenta', 1, '2017-03-21 11:26:33', 1),
(15, 'Sale of 2 units for Fish Capenta', 1, '2017-03-21 11:35:10', 1),
(16, 'Sale of 2 units for Fish Capenta', 1, '2017-03-21 11:39:55', 1),
(17, 'Sale of 1 units for Fish Capenta', 1, '2017-03-21 11:58:25', 1),
(18, 'Sale of 1 units for Fish Capenta', 1, '2017-03-21 11:58:58', 1),
(19, 'Sale of 3 units for Fish Capenta', 1, '2017-03-21 12:00:04', 1),
(20, 'Sale of 1 units for Fish Capenta', 1, '2017-03-21 12:03:45', 2),
(21, 'Sale of 1 units for Fish Capenta', 1, '2018-01-02 23:09:45', 1),
(22, 'Sale of 1 units for Matemba', 2, '2018-01-02 23:10:15', 1),
(23, 'Sale of 3 units for Fish Capenta', 1, '2018-01-05 12:57:06', 1),
(24, 'Sale of 12 units for Fish Capenta', 1, '2018-01-05 13:00:21', 2),
(25, 'New Take on Balance for Fish Capenta set from -15 to 1', 1, '2018-01-05 13:10:01', 1),
(26, 'Purchase of 1 units for Fish Capenta @ a cost of 20.', 1, '2018-01-09 04:43:09', 1),
(27, 'Sale of 20 units for Fish Capenta', 1, '2018-01-09 04:45:02', 1),
(28, 'Purchase of 30 units for Fish Capenta @ a cost of 20.', 1, '2018-01-09 04:46:16', 1),
(29, 'Sale of 1 units for Fish Capenta', 1, '2018-03-06 17:14:38', 1),
(30, 'Sale of 1 units for Matemba', 2, '2018-03-06 17:14:38', 1),
(31, 'Sale of 1 units for Fish Capenta', 1, '2018-11-26 19:03:22', 1),
(32, 'Sale of 5 units for Airtime Credit', 3, '2018-11-27 03:57:20', 1),
(33, 'Sale of 200 units for Electricity Voucher', 4, '2018-11-27 03:57:20', 1),
(34, 'Sale of 1 units for Airtime Credit', 3, '2018-11-27 04:34:47', 1),
(35, 'Sale of 5 units for Airtime Credit', 3, '2018-11-27 04:44:24', 1),
(36, 'Sale of 1200 units for Electricity Voucher', 4, '2018-11-27 04:44:28', 1),
(37, 'Sale of 5 units for Airtime Credit', 3, '2018-11-27 04:50:14', 1),
(38, 'Sale of 1200 units for Electricity Voucher', 4, '2018-11-27 04:50:17', 1),
(39, 'Sale of 5 units for Airtime Credit', 3, '2018-11-27 04:54:18', 1),
(40, 'Sale of 1200 units for Electricity Voucher', 4, '2018-11-27 04:54:21', 1),
(41, 'Sale of 3 units for Airtime Credit', 3, '2018-12-02 18:07:14', 1),
(42, 'Sale of 6 units for Electricity Voucher', 4, '2018-12-02 18:07:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `receipt_header_info`
--

CREATE TABLE `receipt_header_info` (
  `id` int(11) NOT NULL,
  `Receipt_no` varchar(200) DEFAULT NULL,
  `Customer` varchar(200) DEFAULT NULL,
  `NetValue` decimal(10,2) DEFAULT NULL,
  `TotalDiscount` decimal(10,4) DEFAULT NULL,
  `Payable_amount` decimal(10,2) DEFAULT NULL,
  `Actual_Payment` decimal(10,2) DEFAULT NULL,
  `Outstanding_balance` decimal(10,2) DEFAULT NULL,
  `payment_mode` varchar(20) DEFAULT NULL,
  `Status` varchar(50) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` int(11) DEFAULT NULL,
  `LastUpdatedReason` varchar(300) DEFAULT NULL,
  `LastUpdateDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `LastUpdateBy` int(11) DEFAULT NULL,
  `fiscal_signature` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt_header_info`
--

INSERT INTO `receipt_header_info` (`id`, `Receipt_no`, `Customer`, `NetValue`, `TotalDiscount`, `Payable_amount`, `Actual_Payment`, `Outstanding_balance`, `payment_mode`, `Status`, `CreatedDate`, `CreatedBy`, `LastUpdatedReason`, `LastUpdateDate`, `LastUpdateBy`, `fiscal_signature`) VALUES
(30, 'Axi20181127034424', 'Takunda', '33.35', '0.0000', '33.35', '33.35', '0.00', 'cash', 'Sold', '2018-11-27 04:44:31', 1, NULL, NULL, NULL, 'AF-F3-60-73-CF-0A-FA-8C-B7-BC-BB-E9-16-AB-79-5D-AF-1C-9D-B0-A2-57-81-D8-D6-21-C1-F3-9F-9D-B1-02'),
(31, 'Axi20181127035014', 'Takunda', '33.35', '0.0000', '33.35', '33.35', '0.00', 'cash', 'Sold', '2018-11-27 04:50:20', 1, NULL, NULL, NULL, 'AF-F3-60-73-CF-0A-FA-8C-B7-BC-BB-E9-16-AB-79-5D-AF-1C-9D-B0-A2-57-81-D8-D6-21-C1-F3-9F-9D-B1-02'),
(32, 'Axi20181127035418', 'takunda', '33.35', '0.0000', '33.35', '33.35', '0.00', 'cash', 'Sold', '2018-11-27 04:54:25', 1, NULL, NULL, NULL, 'AF-F3-60-73-CF-0A-FA-8C-B7-BC-BB-E9-16-AB-79-5D-AF-1C-9D-B0-A2-57-81-D8-D6-21-C1-F3-9F-9D-B1-02'),
(33, 'Axi20181202170714', 'Takunda', '3.59', '0.0000', '3.59', '3.59', '0.00', 'cash', 'Sold', '2018-12-02 18:07:14', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_details`
--

CREATE TABLE `stock_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `stock_id` varchar(120) DEFAULT NULL,
  `stock_name` varchar(120) DEFAULT NULL,
  `stock_quatity` int(11) DEFAULT NULL,
  `Tax_Code` varchar(10) DEFAULT NULL,
  `company_price` decimal(10,4) DEFAULT NULL,
  `selling_price` decimal(10,4) DEFAULT NULL,
  `category` varchar(120) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CreatedBy` int(11) DEFAULT NULL,
  `expire_date` datetime NOT NULL,
  `uom` varchar(120) NOT NULL,
  `status` varchar(20) DEFAULT 'active',
  `moq` int(11) DEFAULT '10' COMMENT 'Minimum order quantity for an SKI',
  `prescrip` varchar(4900) DEFAULT NULL COMMENT 'prescripption'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_details`
--

INSERT INTO `stock_details` (`id`, `stock_id`, `stock_name`, `stock_quatity`, `Tax_Code`, `company_price`, `selling_price`, `category`, `date`, `CreatedBy`, `expire_date`, `uom`, `status`, `moq`, `prescrip`) VALUES
(3, 'Airtime1', 'Airtime Credit', 976, 'A', '1.0000', '1.0000', 'Data and Airtime', '2019-04-05 04:36:06', 1, '0000-00-00 00:00:00', 'EA', 'active', 10, 'mushonga wenyama'),
(4, 'Electricity-voucher-009', 'Electricity Voucher', 16194, 'A', '0.0500', '0.0150', 'Electricity Services', '2019-04-05 04:36:11', 1, '0000-00-00 00:00:00', 'kWh', 'active', 10, 'wembumbzdi'),
(5, 'Par001', 'Paracetamol', 5, 'A', '10.0000', '200.0000', 'Foods', '2019-04-05 04:37:35', 1, '2019-04-27 00:00:00', 'Kg', 'active', 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_entries`
--

CREATE TABLE `stock_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `transID` varchar(120) DEFAULT NULL,
  `stock_id` varchar(120) NOT NULL,
  `stock_name` varchar(260) NOT NULL,
  `stock_supplier_name` varchar(200) NOT NULL,
  `category` varchar(120) NOT NULL,
  `quantity` decimal(10,4) DEFAULT NULL,
  `Opening_Stock` decimal(10,4) DEFAULT NULL,
  `Closing_Stock` decimal(10,4) DEFAULT NULL,
  `company_price` decimal(10,2) NOT NULL,
  `selling_price` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `mode` varchar(150) NOT NULL,
  `payment` decimal(10,2) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `Date_Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DoneBy` int(11) DEFAULT NULL,
  `notes_to_status` varchar(300) DEFAULT NULL,
  `date_to_status` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `DoneBy_to_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_entries`
--

INSERT INTO `stock_entries` (`id`, `transID`, `stock_id`, `stock_name`, `stock_supplier_name`, `category`, `quantity`, `Opening_Stock`, `Closing_Stock`, `company_price`, `selling_price`, `total`, `mode`, `payment`, `balance`, `due_date`, `type`, `status`, `Date_Created`, `DoneBy`, `notes_to_status`, `date_to_status`, `DoneBy_to_status`) VALUES
(1, 'PO1', 'AirTime1', 'Airtime Credit', 'G-Expresso', 'Data and Airtime', '10.0000', '1.0000', '11.0000', '2.00', '1.00', '20.00', 'cash', '20.00', '0.00', '2018-01-09 00:00:00', 'Invoice', 'Purchased', '2018-01-09 04:43:09', 1, NULL, '2018-11-27 03:51:00', NULL),
(2, 'PO2', 'Electricity Voucher', 'Electricity Token', 'ESCOM', 'Electricity and Power', '30.0000', '50.0000', '80.0000', '20.00', '0.50', '600.00', 'partly_cr', '400.00', '200.00', '2018-01-09 00:00:00', 'Invoice', 'Purchased', '2018-01-09 04:46:16', 1, NULL, '2018-11-27 03:50:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_movement`
--

CREATE TABLE `stock_movement` (
  `id` int(11) NOT NULL,
  `ProductCode` varchar(200) DEFAULT NULL,
  `Quantity` decimal(10,4) DEFAULT NULL,
  `Type` varchar(30) DEFAULT NULL,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_movement`
--

INSERT INTO `stock_movement` (`id`, `ProductCode`, `Quantity`, `Type`, `DateCreated`, `CreatedBy`) VALUES
(1, 'Fish3kg', '1.0000', 'Sale', '2017-03-15 12:17:58', 1),
(2, 'Fish3kg', '10.0000', 'Sale', '2017-03-21 09:54:01', 1),
(3, 'Fish3kg', '10.0000', 'Sale', '2017-03-21 09:57:54', 1),
(4, 'Fish3kg', '1.0000', 'Sale', '2017-03-21 09:59:44', 1),
(5, 'Fish3kg', '2.0000', 'Sale', '2017-03-21 10:00:26', 1),
(6, 'Fish3kg', '1.0000', 'Sale', '2017-03-21 10:03:37', 1),
(7, 'Fish3kg', '2.0000', 'Sale', '2017-03-21 10:11:14', 1),
(8, 'Fish3kg', '2.0000', 'Sale', '2017-03-21 10:11:37', 1),
(9, 'Fish3kg', '2.0000', 'Sale', '2017-03-21 10:12:08', 1),
(10, 'Fish3kg', '1.0000', 'Sale', '2017-03-21 11:16:06', 1),
(11, 'Fish3kg', '1.0000', 'Sale', '2017-03-21 11:16:37', 1),
(12, 'Fish3kg', '1.0000', 'Sale', '2017-03-21 11:17:58', 1),
(13, 'Fish3kg', '2.0000', 'Sale', '2017-03-21 11:20:51', 1),
(14, 'Fish3kg', '1.0000', 'Sale', '2017-03-21 11:26:33', 1),
(15, 'Fish3kg', '2.0000', 'Sale', '2017-03-21 11:35:09', 1),
(16, 'Fish3kg', '2.0000', 'Sale', '2017-03-21 11:39:55', 1),
(17, 'Fish3kg', '1.0000', 'Sale', '2017-03-21 11:58:24', 1),
(18, 'Fish3kg', '1.0000', 'Sale', '2017-03-21 11:58:58', 1),
(19, 'Fish3kg', '3.0000', 'Sale', '2017-03-21 12:00:04', 1),
(20, 'Fish3kg', '1.0000', 'Sale', '2017-03-21 12:03:44', 2),
(21, 'Fish3kg', '1.0000', 'Sale', '2018-01-02 23:09:45', 1),
(22, '89900', '1.0000', 'Sale', '2018-01-02 23:10:15', 1),
(23, 'OVRFish3kg', '3.0000', 'Sale', '2018-01-05 12:57:06', 1),
(24, 'OVRFish3kg', '12.0000', 'Sale', '2018-01-05 13:00:21', 2),
(25, 'OVRFish3kg', '1.0000', 'Purchase', '2018-01-09 04:43:09', 1),
(26, 'OVRFish3kg', '20.0000', 'Sale', '2018-01-09 04:45:02', 1),
(27, 'OVRFish3kg', '30.0000', 'Purchase', '2018-01-09 04:46:16', 1),
(28, 'OVRFish3kg', '1.0000', 'Sale', '2018-03-06 17:14:38', 1),
(29, '89900', '1.0000', 'Sale', '2018-03-06 17:14:38', 1),
(30, 'OVRFish3kg', '1.0000', 'Sale', '2018-11-26 19:03:22', 1),
(31, 'Airtime1', '5.0000', 'Sale', '2018-11-27 03:57:20', 1),
(32, 'Electricity-voucher-009', '200.0000', 'Sale', '2018-11-27 03:57:20', 1),
(33, 'Airtime1', '1.0000', 'Sale', '2018-11-27 04:34:47', 1),
(34, 'Airtime1', '5.0000', 'Sale', '2018-11-27 04:44:24', 1),
(35, 'Electricity-voucher-009', '1200.0000', 'Sale', '2018-11-27 04:44:28', 1),
(36, 'Airtime1', '5.0000', 'Sale', '2018-11-27 04:50:14', 1),
(37, 'Electricity-voucher-009', '1200.0000', 'Sale', '2018-11-27 04:50:17', 1),
(38, 'Airtime1', '5.0000', 'Sale', '2018-11-27 04:54:18', 1),
(39, 'Electricity-voucher-009', '1200.0000', 'Sale', '2018-11-27 04:54:21', 1),
(40, 'Airtime1', '3.0000', 'Sale', '2018-12-02 18:07:14', 1),
(41, 'Electricity-voucher-009', '6.0000', 'Sale', '2018-12-02 18:07:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock_sales`
--

CREATE TABLE `stock_sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `rec_no` varchar(120) NOT NULL,
  `customer` varchar(120) NOT NULL,
  `stock_id` varchar(100) DEFAULT NULL,
  `stock_name` varchar(200) NOT NULL,
  `category` varchar(120) NOT NULL,
  `Tax_Code` varchar(10) DEFAULT NULL,
  `selling_price` decimal(10,2) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `grand_total` decimal(10,4) NOT NULL,
  `dis_amount` decimal(10,2) NOT NULL,
  `payable_amount` decimal(10,4) DEFAULT NULL,
  `payment` decimal(10,2) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `payment_mode` varchar(250) NOT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(25) DEFAULT NULL,
  `date_to_status` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `Who_to_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_sales`
--

INSERT INTO `stock_sales` (`id`, `rec_no`, `customer`, `stock_id`, `stock_name`, `category`, `Tax_Code`, `selling_price`, `quantity`, `subtotal`, `grand_total`, `dis_amount`, `payable_amount`, `payment`, `balance`, `due_date`, `payment_mode`, `CreatedBy`, `CreatedDate`, `status`, `date_to_status`, `Who_to_status`) VALUES
(32, 'Axi20181127034424', 'Takunda', 'Airtime1', 'Airtime Credit', 'Data and Airtime', 'A', '1.00', '5.00', '5.00', '33.3500', '0.00', '33.3500', '33.35', '0.00', '2018-11-27 00:00:00', 'cash', 1, '2018-11-27 04:44:24', 'Sold', '0000-00-00 00:00:00', NULL),
(33, 'Axi20181127034424', 'Takunda', 'Electricity-voucher-009', 'Electricity Voucher', 'Electricity Services', 'A', '0.02', '1200.00', '24.00', '33.3500', '0.00', '33.3500', '33.35', '0.00', '2018-11-27 00:00:00', 'cash', 1, '2018-11-27 04:44:28', 'Sold', '0000-00-00 00:00:00', NULL),
(34, 'Axi20181127035014', 'Takunda', 'Airtime1', 'Airtime Credit', 'Data and Airtime', 'A', '1.00', '5.00', '5.00', '33.3500', '0.00', '33.3500', '33.35', '0.00', '2018-11-27 00:00:00', 'cash', 1, '2018-11-27 04:50:14', 'Sold', '0000-00-00 00:00:00', NULL),
(35, 'Axi20181127035014', 'Takunda', 'Electricity-voucher-009', 'Electricity Voucher', 'Electricity Services', 'A', '0.02', '1200.00', '24.00', '33.3500', '0.00', '33.3500', '33.35', '0.00', '2018-11-27 00:00:00', 'cash', 1, '2018-11-27 04:50:17', 'Sold', '0000-00-00 00:00:00', NULL),
(36, 'Axi20181127035418', 'takunda', 'Airtime1', 'Airtime Credit', 'Data and Airtime', 'A', '1.00', '5.00', '5.00', '33.3500', '0.00', '33.3500', '33.35', '0.00', '2018-11-27 00:00:00', 'cash', 1, '2018-11-27 04:54:18', 'Sold', '0000-00-00 00:00:00', NULL),
(37, 'Axi20181127035418', 'takunda', 'Electricity-voucher-009', 'Electricity Voucher', 'Electricity Services', 'A', '0.02', '1200.00', '24.00', '33.3500', '0.00', '33.3500', '33.35', '0.00', '2018-11-27 00:00:00', 'cash', 1, '2018-11-27 04:54:21', 'Sold', '0000-00-00 00:00:00', NULL),
(38, 'Axi20181202170714', 'Takunda', 'Airtime1', 'Airtime Credit', 'Data and Airtime', 'A', '1.00', '3.00', '3.00', '3.5880', '0.00', '3.5880', '3.59', '0.00', '2018-12-02 00:00:00', 'cash', 1, '2018-12-02 18:07:14', 'Sold', '0000-00-00 00:00:00', NULL),
(39, 'Axi20181202170714', 'Takunda', 'Electricity-voucher-009', 'Electricity Voucher', 'Electricity Services', 'A', '0.02', '6.00', '0.12', '3.5880', '0.00', '3.5880', '3.59', '0.00', '2018-12-02 00:00:00', 'cash', 1, '2018-12-02 18:07:14', 'Sold', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_user`
--

CREATE TABLE `stock_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `answer` varchar(100) NOT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_user`
--

INSERT INTO `stock_user` (`id`, `username`, `password`, `user_type`, `answer`, `CreatedBy`, `CreatedDate`) VALUES
(1, 'admin', '12345', 'admin', 'mycat', NULL, '2017-03-15 12:11:13'),
(2, 'Test', '12345', 'user', 'user', 1, '2017-03-21 12:01:50'),
(3, 'me', 'me', 'user', 'user', 1, '2018-01-05 12:06:28'),
(4, 'TATATA', 'TATATA', 'user', 'user', 1, '2018-07-12 21:28:15');

-- --------------------------------------------------------

--
-- Table structure for table `store_details`
--

CREATE TABLE `store_details` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `log` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tax_condition` varchar(100) NOT NULL,
  `vat` varchar(100) NOT NULL,
  `bpn` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_details`
--

INSERT INTO `store_details` (`id`, `name`, `log`, `type`, `address`, `city`, `phone`, `email`, `tax_condition`, `vat`, `bpn`) VALUES
(2, 'Axis Solutions Malawi', 'larfag.jpg', '', '14 Arundel Road', 'Blantyre', '07736229282', 'helpdesk@axissol.com', 'VAT_REGISTERED', '11000099211', 794729812);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_details`
--

CREATE TABLE `supplier_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_name` varchar(200) NOT NULL,
  `supplier_address` varchar(500) NOT NULL,
  `supplier_contact1` varchar(100) NOT NULL,
  `supplier_contact2` varchar(100) NOT NULL,
  `balance` int(11) NOT NULL,
  `Date_Created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_details`
--

INSERT INTO `supplier_details` (`id`, `supplier_name`, `supplier_address`, `supplier_contact1`, `supplier_contact2`, `balance`, `Date_Created`) VALUES
(1, 'tandai', '11 Watermeyer drive belvedere Zimbabwe', '', '', 200, '2018-01-09 04:43:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_uom`
--

CREATE TABLE `tbl_uom` (
  `uom_id` int(11) NOT NULL,
  `UOM_DESC` varchar(200) DEFAULT NULL,
  `UOM_Detail` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_uom`
--

INSERT INTO `tbl_uom` (`uom_id`, `UOM_DESC`, `UOM_Detail`) VALUES
(1, 'Kg', 'Kilograms'),
(2, 'Lt', 'Litres');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(50) NOT NULL,
  `customer` varchar(250) NOT NULL,
  `supplier` varchar(250) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `payment` decimal(10,2) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `due` datetime NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rid` varchar(120) NOT NULL,
  `receiptid` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_list_age_data`
--

CREATE TABLE `transaction_list_age_data` (
  `id` int(11) NOT NULL,
  `Supp_Cust_Name` varchar(70) DEFAULT NULL,
  `transID` varchar(20) DEFAULT NULL,
  `Trans_Type` varchar(50) DEFAULT NULL,
  `Transaction_Date` datetime DEFAULT NULL,
  `Trans_Amount` decimal(10,4) DEFAULT NULL,
  `Trans_Class` varchar(60) DEFAULT NULL,
  `Date_Created` datetime DEFAULT CURRENT_TIMESTAMP,
  `Created_By` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_list_age_data`
--

INSERT INTO `transaction_list_age_data` (`id`, `Supp_Cust_Name`, `transID`, `Trans_Type`, `Transaction_Date`, `Trans_Amount`, `Trans_Class`, `Date_Created`, `Created_By`) VALUES
(1, 'tandai', 'PO1', 'Payment', '2018-01-09 00:00:00', '20.0000', 'Purchase', '2018-01-09 04:43:09', 1),
(2, 'tandai', 'PO1', 'Invoice', '2018-01-09 00:00:00', '20.0000', 'Purchase', '2018-01-09 04:43:09', 1),
(3, 'tandai', 'PO2', 'Payment', '2018-01-09 00:00:00', '400.0000', 'Purchase', '2018-01-09 04:46:16', 1),
(4, 'tandai', 'PO2', 'Invoice', '2018-01-09 00:00:00', '600.0000', 'Purchase', '2018-01-09 04:46:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_list_debtorage_data`
--

CREATE TABLE `transaction_list_debtorage_data` (
  `Age_ID` int(11) NOT NULL,
  `CustomerName` varchar(200) DEFAULT NULL,
  `TransID` varchar(100) DEFAULT NULL,
  `TransType` varchar(20) DEFAULT NULL,
  `TransDate` datetime DEFAULT NULL,
  `TransAmnt` decimal(10,4) DEFAULT NULL,
  `TransactionClass` varchar(30) DEFAULT NULL,
  `Created_Date` datetime DEFAULT CURRENT_TIMESTAMP,
  `Logged_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_list_debtorage_data`
--

INSERT INTO `transaction_list_debtorage_data` (`Age_ID`, `CustomerName`, `TransID`, `TransType`, `TransDate`, `TransAmnt`, `TransactionClass`, `Created_Date`, `Logged_by`) VALUES
(1, 'Takunda', 'Mad20170315111758', 'Payment', '2017-03-15 11:17:58', '21.0000', 'Sale', '2017-03-15 12:17:58', 1),
(2, 'Takunda', 'Mad20170315111758', 'Receipt', '2017-03-15 11:17:58', '21.0000', 'Sale', '2017-03-15 12:17:58', 1),
(3, 'Takunda', 'Tre20170321085401', 'Payment', '2017-03-21 08:54:01', '288.0000', 'Sale', '2017-03-21 09:54:01', 1),
(4, 'Takunda', 'Tre20170321085401', 'Receipt', '2017-03-21 08:54:01', '288.0000', 'Sale', '2017-03-21 09:54:01', 1),
(5, 'Takunda', 'Tre20170321085753', 'Payment', '2017-03-21 08:57:53', '240.0000', 'Sale', '2017-03-21 09:57:54', 1),
(6, 'Takunda', 'Tre20170321085753', 'Receipt', '2017-03-21 08:57:53', '240.0000', 'Sale', '2017-03-21 09:57:54', 1),
(7, 'Takunda', 'Tre20170321085944', 'Payment', '2017-03-21 08:59:44', '30.0000', 'Sale', '2017-03-21 09:59:44', 1),
(8, 'Takunda', 'Tre20170321085944', 'Receipt', '2017-03-21 08:59:44', '30.0000', 'Sale', '2017-03-21 09:59:44', 1),
(9, 'Takunda', 'Tre20170321090026', 'Payment', '2017-03-21 09:00:26', '60.0000', 'Sale', '2017-03-21 10:00:26', 1),
(10, 'Takunda', 'Tre20170321090026', 'Receipt', '2017-03-21 09:00:26', '60.0000', 'Sale', '2017-03-21 10:00:26', 1),
(11, 'Takunda', 'Tre20170321090337', 'Payment', '2017-03-21 09:03:37', '30.0000', 'Sale', '2017-03-21 10:03:38', 1),
(12, 'Takunda', 'Tre20170321090337', 'Receipt', '2017-03-21 09:03:37', '30.0000', 'Sale', '2017-03-21 10:03:38', 1),
(13, 'Takunda', 'Tre20170321091114', 'Payment', '2017-03-21 09:11:14', '57.6000', 'Sale', '2017-03-21 10:11:14', 1),
(14, 'Takunda', 'Tre20170321091114', 'Receipt', '2017-03-21 09:11:14', '57.6000', 'Sale', '2017-03-21 10:11:14', 1),
(15, 'Takunda', 'Tre20170321091137', 'Payment', '2017-03-21 09:11:37', '57.6000', 'Sale', '2017-03-21 10:11:37', 1),
(16, 'Takunda', 'Tre20170321091137', 'Receipt', '2017-03-21 09:11:37', '57.6000', 'Sale', '2017-03-21 10:11:37', 1),
(17, 'Takunda', 'Tre20170321091208', 'Payment', '2017-03-21 09:12:08', '57.6000', 'Sale', '2017-03-21 10:12:08', 1),
(18, 'Takunda', 'Tre20170321091208', 'Receipt', '2017-03-21 09:12:08', '57.6000', 'Sale', '2017-03-21 10:12:08', 1),
(19, 'Takunda', 'Tre20170321101606', 'Payment', '2017-03-21 10:16:06', '33.1200', 'Sale', '2017-03-21 11:16:06', 1),
(20, 'Takunda', 'Tre20170321101606', 'Receipt', '2017-03-21 10:16:06', '33.1200', 'Sale', '2017-03-21 11:16:06', 1),
(21, 'Takunda', 'Tre20170321101637', 'Payment', '2017-03-21 10:16:37', '33.4650', 'Sale', '2017-03-21 11:16:37', 1),
(22, 'Takunda', 'Tre20170321101637', 'Receipt', '2017-03-21 10:16:37', '33.4650', 'Sale', '2017-03-21 11:16:37', 1),
(23, 'Takunda', 'Tre20170321101758', 'Payment', '2017-03-21 10:17:58', '33.4650', 'Sale', '2017-03-21 11:17:59', 1),
(24, 'Takunda', 'Tre20170321101758', 'Receipt', '2017-03-21 10:17:58', '33.4650', 'Sale', '2017-03-21 11:17:59', 1),
(25, 'Takunda', 'Tre20170321102051', 'Payment', '2017-03-21 10:20:51', '69.0000', 'Sale', '2017-03-21 11:20:51', 1),
(26, 'Takunda', 'Tre20170321102051', 'Receipt', '2017-03-21 10:20:51', '69.0000', 'Sale', '2017-03-21 11:20:51', 1),
(27, 'Takunda', 'Tre20170321102633', 'Payment', '2017-03-21 10:26:33', '34.5000', 'Sale', '2017-03-21 11:26:33', 1),
(28, 'Takunda', 'Tre20170321102633', 'Receipt', '2017-03-21 10:26:33', '34.5000', 'Sale', '2017-03-21 11:26:33', 1),
(29, 'Takunda', 'Tre20170321103509', 'Payment', '2017-03-21 10:35:09', '69.0000', 'Sale', '2017-03-21 11:35:10', 1),
(30, 'Takunda', 'Tre20170321103509', 'Receipt', '2017-03-21 10:35:09', '69.0000', 'Sale', '2017-03-21 11:35:10', 1),
(31, 'Takunda', 'Tre20170321103955', 'Payment', '2017-03-21 10:39:55', '58.6500', 'Sale', '2017-03-21 11:39:55', 1),
(32, 'Takunda', 'Tre20170321103955', 'Receipt', '2017-03-21 10:39:55', '58.6500', 'Sale', '2017-03-21 11:39:55', 1),
(33, 'Takunda', 'Tre20170321105824', 'Payment', '2017-03-21 10:58:24', '33.8100', 'Sale', '2017-03-21 11:58:25', 1),
(34, 'Takunda', 'Tre20170321105824', 'Receipt', '2017-03-21 10:58:24', '33.8100', 'Sale', '2017-03-21 11:58:25', 1),
(35, 'Takunda', 'Tre20170321105857', 'Payment', '2017-03-21 10:58:57', '31.0500', 'Sale', '2017-03-21 11:58:58', 1),
(36, 'Takunda', 'Tre20170321105857', 'Receipt', '2017-03-21 10:58:57', '31.0500', 'Sale', '2017-03-21 11:58:58', 1),
(37, 'Takunda', 'Tre20170321110004', 'Payment', '2017-03-21 11:00:04', '99.3600', 'Sale', '2017-03-21 12:00:04', 1),
(38, 'Takunda', 'Tre20170321110004', 'Receipt', '2017-03-21 11:00:04', '99.3600', 'Sale', '2017-03-21 12:00:04', 1),
(39, 'Takunda', 'Tre20170321110344', 'Payment', '2017-03-21 11:03:44', '27.6000', 'Sale', '2017-03-21 12:03:45', 2),
(40, 'Takunda', 'Tre20170321110344', 'Receipt', '2017-03-21 11:03:44', '27.6000', 'Sale', '2017-03-21 12:03:45', 2),
(41, 'Masendeke', 'Tre20180102220945', 'Payment', '2018-01-02 22:09:45', '30.0000', 'Sale', '2018-01-02 23:09:45', 1),
(42, 'Masendeke', 'Tre20180102220945', 'Receipt', '2018-01-02 22:09:45', '30.0000', 'Sale', '2018-01-02 23:09:45', 1),
(43, 'Masendeke', 'Tre20180102221015', 'Payment', '2018-01-02 22:10:15', '1.0000', 'Sale', '2018-01-02 23:10:15', 1),
(44, 'Masendeke', 'Tre20180102221015', 'Receipt', '2018-01-02 22:10:15', '1.0000', 'Sale', '2018-01-02 23:10:15', 1),
(45, 'Takunda', 'Tre20180105115706', 'Payment', '2018-01-05 11:57:06', '103.5000', 'Sale', '2018-01-05 12:57:06', 1),
(46, 'Takunda', 'Tre20180105115706', 'Receipt', '2018-01-05 11:57:06', '103.5000', 'Sale', '2018-01-05 12:57:06', 1),
(47, 'Taku Madzamba', 'Tre20180105120021', 'Payment', '2018-01-05 12:00:21', '414.0000', 'Sale', '2018-01-05 13:00:21', 2),
(48, 'Taku Madzamba', 'Tre20180105120021', 'Receipt', '2018-01-05 12:00:21', '414.0000', 'Sale', '2018-01-05 13:00:21', 2),
(49, 'Masendeke', 'Tre20180109034502', 'Payment', '2018-01-09 03:45:02', '690.0000', 'Sale', '2018-01-09 04:45:02', 1),
(50, 'Masendeke', 'Tre20180109034502', 'Receipt', '2018-01-09 03:45:02', '690.0000', 'Sale', '2018-01-09 04:45:02', 1),
(51, 'Taku Madzamba', 'Tre20180306161438', 'Payment', '2018-03-06 16:14:38', '35.6500', 'Sale', '2018-03-06 17:14:38', 1),
(52, 'Taku Madzamba', 'Tre20180306161438', 'Receipt', '2018-03-06 16:14:38', '35.6500', 'Sale', '2018-03-06 17:14:38', 1),
(53, 'takunda', 'Tre20181126180322', 'Payment', '2018-11-26 18:03:22', '32.7750', 'Sale', '2018-11-26 19:03:22', 1),
(54, 'takunda', 'Tre20181126180322', 'Receipt', '2018-11-26 18:03:22', '32.7750', 'Sale', '2018-11-26 19:03:22', 1),
(55, 'takunda', 'Tre20181127025720', 'Payment', '2018-11-27 02:57:20', '10.3500', 'Sale', '2018-11-27 03:57:20', 1),
(56, 'takunda', 'Tre20181127025720', 'Receipt', '2018-11-27 02:57:20', '10.3500', 'Sale', '2018-11-27 03:57:20', 1),
(57, 'takunda', 'Axi20181127033447', 'Payment', '2018-11-27 03:34:47', '1.1500', 'Sale', '2018-11-27 04:34:51', 1),
(58, 'takunda', 'Axi20181127033447', 'Receipt', '2018-11-27 03:34:47', '1.1500', 'Sale', '2018-11-27 04:34:51', 1),
(59, 'Takunda', 'Axi20181127034424', 'Payment', '2018-11-27 03:44:24', '33.3500', 'Sale', '2018-11-27 04:44:31', 1),
(60, 'Takunda', 'Axi20181127034424', 'Receipt', '2018-11-27 03:44:24', '33.3500', 'Sale', '2018-11-27 04:44:31', 1),
(61, 'Takunda', 'Axi20181127035014', 'Payment', '2018-11-27 03:50:14', '33.3500', 'Sale', '2018-11-27 04:50:20', 1),
(62, 'Takunda', 'Axi20181127035014', 'Receipt', '2018-11-27 03:50:14', '33.3500', 'Sale', '2018-11-27 04:50:20', 1),
(63, 'takunda', 'Axi20181127035418', 'Payment', '2018-11-27 03:54:18', '33.3500', 'Sale', '2018-11-27 04:54:25', 1),
(64, 'takunda', 'Axi20181127035418', 'Receipt', '2018-11-27 03:54:18', '33.3500', 'Sale', '2018-11-27 04:54:25', 1),
(65, 'Takunda', 'Axi20181202170714', 'Payment', '2018-12-02 17:07:14', '3.5880', 'Sale', '2018-12-02 18:07:14', 1),
(66, 'Takunda', 'Axi20181202170714', 'Receipt', '2018-12-02 17:07:14', '3.5880', 'Sale', '2018-12-02 18:07:14', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_details`
--
ALTER TABLE `category_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_details`
--
ALTER TABLE `customer_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pos_response`
--
ALTER TABLE `pos_response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prod_audit`
--
ALTER TABLE `prod_audit`
  ADD PRIMARY KEY (`audit_id`),
  ADD UNIQUE KEY `audit_id` (`audit_id`);

--
-- Indexes for table `receipt_header_info`
--
ALTER TABLE `receipt_header_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_details`
--
ALTER TABLE `stock_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_entries`
--
ALTER TABLE `stock_entries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_movement`
--
ALTER TABLE `stock_movement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_sales`
--
ALTER TABLE `stock_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_user`
--
ALTER TABLE `stock_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_details`
--
ALTER TABLE `store_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_details`
--
ALTER TABLE `supplier_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_uom`
--
ALTER TABLE `tbl_uom`
  ADD PRIMARY KEY (`uom_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_list_age_data`
--
ALTER TABLE `transaction_list_age_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_list_debtorage_data`
--
ALTER TABLE `transaction_list_debtorage_data`
  ADD PRIMARY KEY (`Age_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category_details`
--
ALTER TABLE `category_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer_details`
--
ALTER TABLE `customer_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pos_response`
--
ALTER TABLE `pos_response`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `prod_audit`
--
ALTER TABLE `prod_audit`
  MODIFY `audit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `receipt_header_info`
--
ALTER TABLE `receipt_header_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `stock_details`
--
ALTER TABLE `stock_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stock_entries`
--
ALTER TABLE `stock_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock_movement`
--
ALTER TABLE `stock_movement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `stock_sales`
--
ALTER TABLE `stock_sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `stock_user`
--
ALTER TABLE `stock_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `store_details`
--
ALTER TABLE `store_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `supplier_details`
--
ALTER TABLE `supplier_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_uom`
--
ALTER TABLE `tbl_uom`
  MODIFY `uom_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction_list_age_data`
--
ALTER TABLE `transaction_list_age_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaction_list_debtorage_data`
--
ALTER TABLE `transaction_list_debtorage_data`
  MODIFY `Age_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
